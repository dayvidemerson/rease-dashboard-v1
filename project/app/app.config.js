'use strict';
angular.module('rease')
    .config(config);

config.$inject = ['$stateProvider', '$httpProvider', '$locationProvider', '$urlRouterProvider', '$ocLazyLoadProvider'];

function config($stateProvider, $httpProvider, $locationProvider, $urlRouterProvider, $ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        modules: [{
            name: 'navbar',
            files: ['app/directives/js/navbar.js']
        },
        {
            name: 'alert',
            files: ['app/directives/js/alert.js']
        },
        {
            name: 'panel',
            files: ['app/directives/js/panel.js']
        },
        {
            name: 'modal',
            files: ['app/directives/js/modal.js']
        },
        {
            name: 'accordion',
            files: ['app/directives/js/accordion.js']
        },
        {
            name: 'confirm-click',
            files: ['app/directives/js/confirm-click.js']
        },
        {
            name: 'loading',
            files: ['app/directives/js/loading.js']
        },
        {
            name: 'file-reader',
            files: ['app/directives/js/file-reader.js']
        }]
    });
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/');
    $stateProvider
    .state('login', {
        url: '/',
        views: {
            '': {
                controller: 'LoginController',
                controllerAs: 'vm',
                templateUrl: 'app/views/users/login.html'
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    serie: true,
                    cache: true,
                    files: [
                        'app/controllers/login.js'
                    ]
                });
            }]
        }
    })
    .state('logout', {
        url: '/logout/',
        views: {
            '': {
                controller: 'LogoutController',
                controllerAs: 'vm',
                template: ''
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'app/controllers/logout.js'
                ]);
            }]
        }
    })
    .state('eventos', {
        url: '/eventos/',
        views: {
            '': {
                controller: 'EventoController',
                controllerAs: 'vm',
                templateUrl: 'app/views/eventos/index.html'
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'app/models/evento.js',
                    'app/controllers/evento.js'
                ]);
            }]
        }
    })
    .state('areas', {
        url: '/areas/',
        views: {
            '': {
                controller: 'AreaController',
                controllerAs: 'vm',
                templateUrl: 'app/views/areas/index.html'
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'app/models/area.js',
                    'app/controllers/area.js'
                ]);
            }]
        }
    })
    .state('profissionais', {
        url: '/profissionais/',
        views: {
            '': {
                controller: 'ProfissionalController',
                controllerAs: 'vm',
                templateUrl: 'app/views/profissionais/index.html'
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'app/models/profissional.js',
                    'app/controllers/profissional.js'
                ]);
            }]
        }
    })
    .state('assinaturas', {
        url: '/assinaturas/',
        views: {
            '': {
                controller: 'AssinaturaDigitalController',
                controllerAs: 'vm',
                templateUrl: 'app/views/assinaturas/index.html'
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'app/models/assinatura-digital.js',
                    'app/controllers/assinatura-digital.js'
                ]);
            }]
        }
    })
    .state('edicoes', {
        url: '/edicoes/{evento:int}',
        views: {
            '': {
                controller: 'EdicaoController',
                controllerAs: 'vm',
                templateUrl: 'app/views/edicoes/index.html'
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'app/models/evento.js',
                    'app/models/area.js',
                    'app/models/edicao.js',
                    'app/controllers/edicao.js'
                ]);
            }]
        }
    })
    .state('configuracaoEdicao', {
        url: '/edicoes/configuracao/{edicao:int}',
        views: {
            '': {
                controller: 'EdicaoConfiguracaoController',
                controllerAs: 'vm',
                templateUrl: 'app/views/edicoes/configuracao.html'
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'app/models/edicao.js',
                    'app/models/tipo-documento.js',
                    'app/models/tipo-inscricao.js',
                    'app/models/grupo.js',
                    'app/models/profissional.js',
                    'app/models/modelo-certificado-minicurso.js',
                    'app/models/modelo-certificado-palestra.js',
                    'app/models/modelo-certificado-atividade.js',
                    'app/models/turma-minicurso.js',
                    'app/models/turma-atividade.js',
                    'app/models/horario-minicurso.js',
                    'app/models/horario-atividade.js',
                    'app/models/minicurso.js',
                    'app/models/palestra.js',
                    'app/models/atividade-geral.js',
                    'app/models/atividade.js',
                    'app/models/assinatura-digital.js',
                    'app/models/configuracao-certificado.js',
                    'app/controllers/edicao.js',
                    'app/controllers/tipo-inscricao.js',
                    'app/controllers/grupo.js',
                    'app/controllers/minicurso.js',
                    'app/controllers/palestra.js',
                    'app/controllers/atividade.js',
                    'app/controllers/configuracao-certificado.js',
                    'app/controllers/edicao-configuracao.js'
                ]);
            }]
        }
    })
    .state('inscricoes', {
        url: '/inscricoes/{edicao:int}',
        views: {
            '': {
                controller: 'InscricaoController',
                controllerAs: 'vm',
                templateUrl: 'app/views/inscricoes/index.html'
            }
        },
        resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'app/services/lista-presenca.js',
                    'app/models/edicao.js',
                    'app/models/minicurso.js',
                    'app/models/atividade.js',
                    'app/models/atividade-geral.js',
                    'app/models/participacao.js',
                    'app/models/palestra.js',
                    'app/models/inscricao.js',
                    'app/models/documento.js',
                    'app/models/certificado.js',
                    'app/controllers/inscricao.js'
                ]);
            }]
        }
    });
}
