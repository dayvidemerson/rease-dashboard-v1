'use strict'
angular.module('rease')
  .run(run)

run.$inject = ['$http', '$state', '$cookies', '$rootScope', 'AuthenticationService']

function run($http, $state, $cookies, $rootScope, AuthenticationService) {
  // Menu Navbar
  $rootScope.menu = [
    {
      label: 'Evento',
      state: 'eventos'
    },
    {
      label: 'Area',
      state: 'areas'
    },
    {
      label: 'Profissional',
      state: 'profissionais'
    },
    {
      label: 'Assinatura',
      state: 'assinaturas'
    }
  ]

  // Token Autenticação
  var token = $cookies.get('token')
  if (token) {
    $rootScope.token = token
    $http.defaults.headers.common.Authorization = 'Token ' + token
  }

  // Redirecionar para Página Inicial se não estiver logado
  $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    if (!AuthenticationService.isLogged() && toState.name != 'login') {
      $state.go('login')
    } else if (AuthenticationService.isLogged() && toState.name == 'login') {
      $state.go('eventos')
    }
    $rootScope.loading = true
  })
}