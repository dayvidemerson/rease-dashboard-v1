'use strict'
angular.module('rease-participante')
  .controller('CertificadoController', CertificadoController)

CertificadoController.$inject = ['CertificadoMinicurso', 'CertificadoAtividade', 'CertificadoPalestra', 'CertificadoService']

function CertificadoController(CertificadoMinicurso, CertificadoAtividade, CertificadoPalestra, CertificadoService) {
  var vm = this
  vm.message = []
  vm.data = {}
  vm.list_loading = false
  vm.certificados = false
  vm.certificados_minicurso = []
  vm.certificados_atividade = []
  vm.certificados_palestra = []

  CertificadoMinicurso.all()
    .then(function(response) {
      var length = response.data.length
      for (var i = 0; i < length; i++) {
        vm.certificados_minicurso.push(new CertificadoMinicurso(response.data[i]))
      }
      if (length) {
        vm.certificados = true
      }
      vm.list_loading = true
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })

  CertificadoAtividade.all()
    .then(function(response) {
      var length = response.data.length
      for (var i = 0; i < length; i++) {
        vm.certificados_atividade.push(new CertificadoAtividade(response.data[i]))
      }
      if (length) {
        vm.certificados = true
      }
      vm.list_loading = true
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })

  CertificadoPalestra.all()
    .then(function(response) {
      var length = response.data.length
      for (var i = 0; i < length; i++) {
        vm.certificados_palestra.push(new CertificadoPalestra(response.data[i]))
      }
      if (length) {
        vm.certificados = true
      }
      vm.list_loading = true
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })

  vm.getCertificado = function(pk) {
    CertificadoService.getCertificado(pk)
      .then(function(response) {
        window.open("data:application/pdf;base64,"+response.data, 'Certificado')
      }, function(reject) {
        vm.message.push({
          text: reject.data,
          tipo: "danger"
        })
      })
  }
}