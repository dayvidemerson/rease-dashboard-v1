'use strict';
angular.module('rease')
  .controller('ConfiguracaoCertificadoController', ConfiguracaoCertificadoController);

ConfiguracaoCertificadoController.$inject = ['ConfiguracaoCertificado', 'ModeloCertificadoMinicurso', 'ModeloCertificadoPalestra', 'ModeloCertificadoAtividade', 'AssinaturaDigital', '$stateParams'];

function ConfiguracaoCertificadoController(ConfiguracaoCertificado, ModeloCertificadoMinicurso, ModeloCertificadoPalestra, ModeloCertificadoAtividade, AssinaturaDigital, $stateParams) {
  var vm = this
  vm.message = []
  vm.assinaturas = []
  vm.data = {}
  vm.error = {}
  vm.configuracao_certificado = null
  vm.modelo_certificado_minicurso = null
  vm.modelo_certificado_palestra = null
  vm.modelo_certificado_comepticao = null
  vm.modelo_certificado_atividade = null

  // Form

  AssinaturaDigital.all()
    .then(function(response) {
      vm.assinaturas = response.data
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })

  vm.clean = function() {
    vm.data = {}
    vm.error = {}
    vm.indexEdit = -1
  }

  vm.cleanMessage = function() {
    vm.message = []
  }

  vm.editConfiguracaoCertificado = function(instance) {
    vm.cleanMessage()
    vm.data = new ConfiguracaoCertificado(instance)
  }

  vm.submitConfiguracaoCertificado = function() {
    var instance = new ConfiguracaoCertificado(vm.data)
    if (vm.formConfiguracaoCertificado.$valid) {
      if (instance.pk) {
        instance.put()
          .then(function(response) {
            vm.configuracao_certificado = instance
            vm.message.push({
              texto: 'Atualizado com sucesso!',
              tipo: 'success'
            })
          }, function(reject) {
            vm.error = reject.data
          })
      } else {
        instance.edicao = $stateParams.edicao
        instance.post()
          .then(function(response) {
            instance = new ConfiguracaoCertificado(response.data)
            vm.configuracao_certificado = instance
            vm.data = instance
            vm.message.push({
              texto: 'Cadastrado com sucesso!',
              tipo: 'success'
            })
          }, function(reject) {
            vm.error = reject.data
          })
      }
    }
  }

  vm.editModeloCertificadoMinicurso = function(instance) {
    vm.cleanMessage()
    vm.data = new ModeloCertificadoMinicurso(instance)
  }

  vm.submitModeloCertificadoMinicurso = function() {
    var instance = new ModeloCertificadoMinicurso(vm.data)
    if (vm.form.$valid) {
      if (instance.pk) {
        instance.put()
          .then(function(response) {
            vm.modelo_certificado_minicurso = instance
            vm.message.push({
              texto: 'Atualizado com sucesso!',
              tipo: 'success'
            })
          }, function(reject) {
            vm.error = reject.data
          })
      } else {
        instance.edicao = $stateParams.edicao
        instance.post()
          .then(function(response) {
            instance = new ModeloCertificadoMinicurso(response.data)
            vm.modelo_certificado_minicurso = instance
            vm.data = instance
            vm.message.push({
              texto: 'Cadastrado com sucesso!',
              tipo: 'success'
            })
          }, function(reject) {
            vm.error = reject.data
          })
      }
    }
  }

  vm.editModeloCertificadoPalestra = function(instance) {
    vm.cleanMessage()
    vm.data = new ModeloCertificadoPalestra(instance)
  }

  vm.submitModeloCertificadoPalestra = function() {
    var instance = new ModeloCertificadoPalestra(vm.data)
    if (vm.form.$valid) {
      if (instance.pk) {
        instance.put()
          .then(function(response) {
            vm.modelo_certificado_palestra = instance
            vm.message.push({
              texto: 'Atualizado com sucesso!',
              tipo: 'success'
            })
          }, function(reject) {
            vm.error = reject.data
          })
      } else {
        instance.edicao = $stateParams.edicao
        instance.post()
          .then(function(response) {
            instance = new ModeloCertificadoPalestra(response.data)
            vm.modelo_certificado_palestra = instance
            vm.data = instance
            vm.message.push({
              texto: 'Cadastrado com sucesso!',
              tipo: 'success'
            })
          }, function(reject) {
            vm.error = reject.data
          })
      }
    }
  }

  vm.editModeloCertificadoAtividade = function(instance) {
    vm.cleanMessage()
    vm.data = new ModeloCertificadoAtividade(instance)
  }

  vm.submitModeloCertificadoAtividade = function() {
    var instance = new ModeloCertificadoAtividade(vm.data)
    if (vm.form.$valid) {
      if (instance.pk) {
        instance.put()
          .then(function(response) {
            vm.modelo_certificado_atividade = instance
            vm.message.push({
              texto: 'Atualizado com sucesso!',
              tipo: 'success'
            })
          }, function(reject) {
            vm.error = reject.data
          })
      } else {
        instance.edicao = $stateParams.edicao
        instance.post()
          .then(function(response) {
            instance = new ModeloCertificadoAtividade(response.data)
            vm.modelo_certificado_atividade = instance
            vm.data = instance
            vm.message.push({
              texto: 'Cadastrado com sucesso!',
              tipo: 'success'
            })
          }, function(reject) {
            vm.error = reject.data
          })
      }
    }
  }

  // List

  ConfiguracaoCertificado.all($stateParams.edicao)
    .then(function(response) {
      if (response.data[0]) {
        vm.configuracao_certificado = new ConfiguracaoCertificado(response.data[0])
      }
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })

  ModeloCertificadoMinicurso.all($stateParams.edicao)
    .then(function(response) {
      if (response.data[0]) {
        vm.modelo_certificado_minicurso = new ModeloCertificadoMinicurso(response.data[0])
      }
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })

  ModeloCertificadoPalestra.all($stateParams.edicao)
    .then(function(response) {
      if (response.data[0]) {
        vm.modelo_certificado_palestra = new ModeloCertificadoPalestra(response.data[0])
      }
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })

  ModeloCertificadoAtividade.all($stateParams.edicao)
    .then(function(response) {
      if (response.data[0]) {
        vm.modelo_certificado_atividade = new ModeloCertificadoAtividade(response.data[0])
      }
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })

  vm.destroyConfiguracaoCertificado = function(instance) {
    instance.destroy()
      .then(function() {
        vm.configuracao_certificado = null
        vm.message.push({
          texto: 'Deletado com sucesso!',
          tipo: 'success'
        })
      }, function(reject) {
        vm.message.push({
          texto: reject.data,
          tipo: 'danger'
        })
      })
  }

  vm.destroyModeloCertificadoMinicurso = function(instance) {
    instance.destroy()
      .then(function() {
        vm.modelo_certificado_minicurso = null
        vm.message.push({
          texto: 'Deletado com sucesso!',
          tipo: 'success'
        })
      }, function(reject) {
        vm.message.push({
          texto: reject.data,
          tipo: 'danger'
        })
      })
  }

  vm.destroyModeloCertificadoPalestra = function(instance) {
    instance.destroy()
      .then(function() {
        vm.modelo_certificado_palestra = null
        vm.message.push({
          texto: 'Deletado com sucesso!',
          tipo: 'success'
        })
      }, function(reject) {
        vm.message.push({
          texto: reject.data,
          tipo: 'danger'
        })
      })
  }

  vm.destroyModeloCertificadoAtividade = function(instance) {
    instance.destroy()
      .then(function() {
        vm.modelo_certificado_atividade = null
        vm.message.push({
          texto: 'Deletado com sucesso!',
          tipo: 'success'
        })
      }, function(reject) {
        vm.message.push({
          texto: reject.data,
          tipo: 'danger'
        })
      })
  }
}