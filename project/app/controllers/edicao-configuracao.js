'use strict';
angular.module('rease')
  .controller('EdicaoConfiguracaoController', EdicaoConfiguracaoController);

EdicaoConfiguracaoController.$inject = ['Edicao', 'EdicaoConfiguracaoService', '$stateParams'];

function EdicaoConfiguracaoController(Edicao, EdicaoConfiguracaoService, $stateParams) {
  var vm = this;
  EdicaoConfiguracaoService.setEdicao($stateParams.edicao);
  vm.edicao = null;
  vm.previous = '';
  vm.openTipoInscricao = true;
  vm.openGrupo = false;
  vm.openMinicurso = false;
  vm.openPalestra = false;
  vm.openAtividade = false;
  vm.openCertificado = false;

  Edicao.get($stateParams.edicao)
    .then(function(response) {
      vm.edicao = new Edicao(response.data);
      vm.previous = 'edicoes({evento: '+vm.edicao.evento+'})';
    });

  vm.showTipoInscricao = function() {
    vm.openTipoInscricao = true;
    vm.openGrupo = false;
    vm.openMinicurso = false;
    vm.openPalestra = false;
    vm.openAtividade = false;
    vm.openCertificado = false;
  }

  vm.showGrupo = function() {
    vm.openTipoInscricao = false;
    vm.openGrupo = true;
    vm.openMinicurso = false;
    vm.openPalestra = false;
    vm.openAtividade = false;
    vm.openCertificado = false;
  }

  vm.showMinicurso = function() {
    vm.openTipoInscricao = false;
    vm.openGrupo = false;
    vm.openMinicurso = true;
    vm.openPalestra = false;
    vm.openAtividade = false;
    vm.openCertificado = false;
  }

  vm.showPalestra = function() {
    vm.openTipoInscricao = false;
    vm.openGrupo = false;
    vm.openMinicurso = false;
    vm.openPalestra = true;
    vm.openAtividade = false;
    vm.openCertificado = false;
  }

  vm.showAtividade = function() {
    vm.openTipoInscricao = false;
    vm.openGrupo = false;
    vm.openMinicurso = false;
    vm.openPalestra = false;
    vm.openAtividade = true;
    vm.openCertificado = false;
  }

  vm.showCertificado = function() {
    vm.openTipoInscricao = false;
    vm.openGrupo = false;
    vm.openMinicurso = false;
    vm.openPalestra = false;
    vm.openAtividade = false;
    vm.openCertificado = true;
  }
}