'use strict';
angular.module('rease')
  .controller('EdicaoController', EdicaoController);

EdicaoController.$inject = ['Evento', 'Edicao', 'Area', 'Endereco', '$scope','$stateParams'];

function EdicaoController(Evento, Edicao, Area, Endereco, $scope, $stateParams) {
  var vm = this;
  vm.loading = false;
  vm.previous = 'eventos';
  vm.message = [];
  vm.error = {};
  vm.list = [];
  vm.evento = null;
  vm.estados = [];
  vm.areas = [];
  vm.openForm = false;
  vm.openList = true;
  vm.indexEdit = -1;

  // Form

  Evento.get($stateParams.evento)
    .then(function(response) {
      vm.evento = new Evento(response.data);
    });

  // Form

  Area.all()
    .then(function(response) {
      vm.areas = response.data;
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      });
    });

  Endereco.getEstados()
    .then(function(response) {
      vm.estados = response.data;
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      });
    });

  $scope.$watch('vm.data.cep', function() {
    if (vm.data && vm.form) {
      if (typeof vm.form.cep != undefined && vm.form.cep.$valid) {
        Endereco.getLocalidade(vm.data.cep)
          .then(function(response) {
            vm.data.logradouro = response.data.logradouro;
            vm.data.bairro = response.data.bairro;
            vm.data.cidade = response.data.localidade;
            vm.data.estado = response.data.uf;
          }, function(reject) {
            vm.error = reject.data;
          });
      }
    }
  }, true);

  var loadForm = function(index) {
    if (index > -1) {
      vm.data = new Edicao(vm.list[index]);
    } else {
      vm.data = {};
    }
  }

  vm.clean = function() {
    vm.error = {};
    vm.indexEdit = -1;
    loadForm(vm.indexEdit);
  }

  vm.create = function() {
    vm.openForm = true;
    vm.openList = false;
    vm.indexEdit = -1;
    loadForm(vm.indexEdit);
  }

  vm.edit = function(instance) {
    vm.openForm = true;
    vm.openList = false;
    vm.indexEdit = vm.list.indexOf(instance);
    loadForm(vm.indexEdit);
  }

  vm.showList = function() {
    vm.openForm = false;
    vm.openList = true;
  }

  vm.submit = function() {
    var instance = new Edicao(vm.data);
    if (vm.form.$valid) {
      if (instance.pk) {
        instance.put()
          .then(function(response) {
            vm.list[vm.indexEdit] = instance;
            vm.message.push({
              texto: 'Atualizado com sucesso!',
              tipo: 'success'
            });
            vm.showList();
            vm.clean();
          }, function(reject) {
            vm.error = reject.data;
          });
      } else {
        instance.evento = vm.evento.pk;
        instance.post()
          .then(function(response) {
            vm.list.push(new Edicao(response.data));
            vm.message.push({
              texto: 'Cadastrado com sucesso!',
              tipo: 'success'
            });
            vm.showList();
            vm.clean();
          }, function(reject) {
            vm.error = reject.data;
          });
      }
    }
  }

  // List

  Edicao.all($stateParams.evento)
    .then(function(response) {
      length = response.data.length
      for (var i = 0; i < length; i++) {
        vm.list.push(new Edicao(response.data[i]));
      }
      vm.loading = true
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      });
    });

  vm.destroy = function(instance) {
    instance.destroy()
      .then(function() {
        var index = vm.list.indexOf(instance);
        vm.list.splice(index, 1);
        vm.message.push({
          texto: 'Deletado com sucesso!',
          tipo: 'success'
        });
      }, function(reject) {
        vm.message.push({
          texto: reject.data,
          tipo: 'danger'
        });
      });
  }
}