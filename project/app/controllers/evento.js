'use strict';
angular.module('rease')
  .controller('EventoController', EventoController);

EventoController.$inject = ['Evento'];

function EventoController(Evento) {
  var vm = this;
  vm.loading = false;
  vm.message = [];
  vm.data = {};
  vm.error = {};
  vm.list = [];
  vm.openForm = false;
  vm.openList = true;
  vm.indexEdit = -1;

  // Form

  var loadForm = function(index) {
    if (index > -1) {
      vm.data = new Evento(vm.list[index]);
    } else {
      vm.data = {};
    }
  }

  vm.clean = function() {
    vm.error = {};
    vm.indexEdit = -1;
    loadForm(vm.indexEdit);
  }

  vm.create = function() {
    vm.openForm = true;
    vm.openList = false;
    vm.indexEdit = -1;
    loadForm(vm.indexEdit);
  }

  vm.edit = function(instance) {
    vm.openForm = true;
    vm.openList = false;
    vm.indexEdit = vm.list.indexOf(instance);
    loadForm(vm.indexEdit);
  }

  vm.showList = function() {
    vm.openForm = false;
    vm.openList = true;
  }

  vm.submit = function() {
    var instance = new Evento(vm.data);
    if (vm.form.$valid) {
      if (instance.pk) {
        instance.put()
          .then(function(response) {
            vm.list[vm.indexEdit] = instance;
            vm.message.push({
              texto: 'Atualizado com sucesso!',
              tipo: 'success'
            });
            vm.showList();
            vm.clean();
          }, function(reject) {
            vm.error = reject.data;
          });
      } else {
        instance.post()
          .then(function(response) {
            vm.list.push(new Evento(response.data));
            vm.message.push({
              texto: 'Cadastrado com sucesso!',
              tipo: 'success'
            });
            vm.showList();
            vm.clean();
          }, function(reject) {
            vm.error = reject.data;
          });
      }
    }
  }

  // List
  
  Evento.all()
    .then(function(response) {
      length = response.data.length;
      for (var i = 0; i < length; i++) {
        vm.list.push(new Evento(response.data[i]));
      }
      vm.loading = true;
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      });
    });

  vm.destroy = function(instance) {
    instance.destroy()
      .then(function() {
        var index = vm.list.indexOf(instance);
        vm.list.splice(index, 1);
        vm.message.push({
          texto: 'Deletado com sucesso!',
          tipo: 'success'
        });
      }, function(reject) {
        vm.message.push({
          texto: reject.data.detail,
          tipo: 'danger'
        });
      });
  }
}