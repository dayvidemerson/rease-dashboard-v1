'use strict'
angular.module('rease')
  .controller('InscricaoController', InscricaoController)

InscricaoController.$inject = ['Inscricao', 'Documento', 'Edicao', 'Participacao', 'Minicurso', 'Atividade', 'Palestra', 'AtividadeGeral', 'Certificado', 'ListaPresencaService', '$stateParams']

function InscricaoController(Inscricao, Documento, Edicao, Participacao, Minicurso, Atividade, Palestra, AtividadeGeral, Certificado, ListaPresencaService, $stateParams) {
  var vm = this;
  vm.loading = false;
  vm.edicao = null;
  vm.relatorioMinicurso = {};
  vm.relatorioPalestra = {};
  vm.relatorioAtividade = {};
  vm.atividades_geral = [];
  vm.previous = '';
  vm.message = [];
  vm.messageDocumento = [];
  vm.openInscricoes = true;
  vm.openMinicursos = false;
  vm.openPalestras = false;
  vm.openAtividades = false;
  

  Edicao.get($stateParams.edicao)
    .then(function(response) {
      vm.edicao = new Edicao(response.data);
      vm.previous = 'edicoes({evento: '+vm.edicao.evento+'})';
    });
  
  var loadRelatorioEdicao = function() {
    vm.relatorio = {};
    Edicao.relatorio($stateParams.edicao)
      .then(function(response) {
        vm.relatorio = response.data;
      });
  }

  var loadInscricoes = function() {
    vm.list = [];
    vm.loading = false;
    Inscricao.all($stateParams.edicao)
      .then(function(response) {
        var length = response.data.length;
        for (var i = 0; i < length; i++) {
          vm.list.push(new Inscricao(response.data[i]));
        }
        vm.loading = true;
      }, function(reject) {
        vm.message.push({
          texto: reject.data.detail,
          tipo: 'danger'
        });
      });
    AtividadeGeral.all($stateParams.edicao)
      .then(function (response) {
        var length = response.data.length;
        for (var i = 0; i < length; i++) {
          vm.atividades_geral.push(new AtividadeGeral(response.data[i]));
        };
      })
    loadRelatorioEdicao();
  }


  var loadRelatorioMinicurso = function() {
    vm.loading = false;
    Minicurso.relatorio($stateParams.edicao)
      .then(function(response) {
        vm.relatorioMinicurso = response.data;
        vm.loading = true;
      });
  }

  var loadRelatorioPalestra = function() {
    vm.loading = false;
    Palestra.relatorio($stateParams.edicao)
      .then(function(response) {
        vm.relatorioPalestra = response.data;
        vm.loading = true;
      });
  }

  var loadRelatorioAtividade = function() {
    vm.loading = false;
    Atividade.relatorio($stateParams.edicao)
      .then(function(response) {
        vm.relatorioAtividade = response.data;
        vm.loading = true;
      });
  }

  vm.showInscricoes = function() {
    vm.openInscricoes = true;
    vm.openMinicursos = false;
    vm.openPalestras = false;
    vm.openAtividades = false;
    loadInscricoes();
  }

  vm.showMinicursos = function() {
    vm.openInscricoes = false;
    vm.openMinicursos = true;
    vm.openPalestras = false;
    vm.openAtividades = false;
    loadRelatorioMinicurso();
  }

  vm.showPalestras = function() {
    vm.openInscricoes = false;
    vm.openMinicursos = false;
    vm.openPalestras = true;
    vm.openAtividades = false;
    loadRelatorioPalestra();
  }

  vm.showAtividades = function() {
    vm.openInscricoes = false;
    vm.openMinicursos = false;
    vm.openPalestras = false;
    vm.openAtividades = true;
    loadRelatorioAtividade();
  }

  vm.cleanDocumento = function() {
    vm.messageDocumento = [];
  }

  loadInscricoes();

  // List

  vm.active = function(instance) {
    instance.active(true)
      .then(function(response) {
        var index = vm.list.indexOf(instance);
        var inscricao = new Inscricao(response.data);
        vm.list[index] = inscricao;
        vm.message.push({
          texto: 'Ativada com sucesso!',
          tipo: 'success'
        });
      }, function(reject) {
        vm.message.push({
          texto: reject.data,
          tipo: 'danger'
        });
      });
  }

  vm.deactive = function(instance) {
    instance.active(false)
      .then(function(response) {
        var index = vm.list.indexOf(instance);
        var inscricao = new Inscricao(response.data);
        vm.list[index] = inscricao;
        vm.message.push({
          texto: 'Desativada com sucesso!',
          tipo: 'success'
        });
      }, function(reject) {
        vm.message.push({
          texto: reject.data,
          tipo: 'danger'
        });
      });
  }

  vm.validate = function(instance) {
    instance.validate(true)
      .then(function(response) {
        var index = Inscricao.getIndexDocumento(vm.list, instance.pk);
        var documento = new Documento(response.data);
        vm.list[index[0]].documentos[index[1]] = documento;
        vm.messageDocumento.push({
          texto: 'Validado com sucesso!',
          tipo: 'success'
        });
      }, function(reject) {
        vm.messageDocumento.push({
          texto: reject.data,
          tipo: 'danger'
        });
      });
  }

  vm.deny = function(instance) {
    instance.validate(false)
      .then(function(response) {
        var index = Inscricao.getIndexDocumento(vm.list, instance.pk);
        var documento = new Documento(response.data);
        vm.list[index[0]].documentos[index[1]] = documento;
        vm.messageDocumento.push({
          texto: 'Negado com sucesso!',
          tipo: 'success'
        });
      }, function(reject) {
        vm.messageDocumento.push({
          texto: reject.data,
          tipo: 'danger'
        });
      });
  }

  vm.availableCertificado = function(participante) {
    var certificado = new Certificado({'pk': participante.certificado, 'disponivel': !participante.disponivel});
    certificado.put()
      .then(function(response) {
        participante.disponivel = !participante.disponivel;
      }, function(reject) {
        console.log(reject);
      });
  }

  vm.availableCertificadoEvento = function (inscricao) {
      var certificado = new Certificado({'pk': inscricao.certificado_evento, 'disponivel': true});
      certificado.put()
        .then(function(response) {
          console.log("Deu Certo")
        }, function(reject) {
          console.log(reject);
        });
  }

  vm.addParticipacao = function (inscricao, atividade_geral) {
    var data = {
      'inscricao': inscricao.pk,
      'atividade_geral': atividade_geral.pk
    }
    var participacao = new Participacao(data);
    participacao.post()
      .then(function (response) {
        console.log("Deu certo");
      }, function (reject) {
        console.log("Deu errado");
      })
  }

  vm.getListaPresencaMinicurso = function(pk) {
    ListaPresencaService.getMinicurso(pk)
      .then(function(response) {
        window.open("data:application/pdf;base64,"+response.data, 'Lista de Presença')
      }, function(reject) {
        vm.message.push({
          text: reject.data,
          tipo: "danger"
        })
      })
  }

  vm.getListaPresencaAtividade = function(pk) {
    ListaPresencaService.getAtividade(pk)
      .then(function(response) {
        window.open("data:application/pdf;base64,"+response.data, 'Lista de Presença')
      }, function(reject) {
        vm.message.push({
          text: reject.data,
          tipo: "danger"
        })
      })
  }

  vm.getListaPresencaPalestra = function(pk) {
    ListaPresencaService.getPalestra(pk)
      .then(function(response) {
        window.open("data:application/pdf;base64,"+response.data, 'Lista de Presença')
      }, function(reject) {
        vm.message.push({
          text: reject.data,
          tipo: "danger"
        })
      })
  }
}