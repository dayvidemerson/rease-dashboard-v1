'use strict';
angular.module('rease')
  .controller('LoginController', LoginController);

LoginController.$inject = ['$state', 'AuthenticationService'];

function LoginController($state, AuthenticationService) {
  var vm = this;
  vm.message = [];
  vm.error = {};
  vm.data = {};
  vm.confirmButton = 'Entrar';
  vm.submit = function(){
    vm.confirmButton = 'Carregando';
    if (vm.form.$valid) {
      AuthenticationService.get(vm.data)
        .then(function(response) {
          AuthenticationService.set(response.data);
          $state.go('eventos');
        }, function(reject) {
          vm.message = reject.data.non_field_errors;
          vm.error = reject.data;
          vm.confirmButton = 'Entrar';
        });
    }
  }
}