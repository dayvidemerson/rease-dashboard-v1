'use strict';
angular.module('rease')
  .controller('LogoutController', LogoutController);

LogoutController.$inject = ['$state', 'AuthenticationService'];

function LogoutController($state, AuthenticationService) {
  AuthenticationService.destroy();
  $state.go('login');
}