'use strict';
angular.module('rease')
  .controller('MinicursoController', MinicursoController);

MinicursoController.$inject = ['Minicurso', 'TurmaMinicurso', 'HorarioMinicurso', 'Grupo', 'Profissional', 'Extra', '$stateParams'];

function MinicursoController(Minicurso, TurmaMinicurso, HorarioMinicurso, Grupo, Profissional, Extra, $stateParams) {
  var vm = this;
  vm.loading = false;
  vm.message = [];
  vm.data = {};
  vm.error = {};
  vm.list = [];
  vm.openForm = false;
  vm.openList = true;
  vm.indexEdit = -1;

  vm.loadingTurma = false;
  vm.listTurma = [];
  vm.openFormTurma = false;
  vm.openListTurma = false;
  vm.indexEditTurma = -1;
  vm.profissionais = [];
  vm.grupos = [];

  var minicurso = 0;

  Grupo.all()
    .then(function(response) {
      var length = response.data.length;
      for (var i = 0; i < length; i++) {
        vm.grupos.push(new Grupo(response.data[i]));
      }
    });

  Profissional.all()
    .then(function(response) {
      var length = response.data.length;
      for (var i = 0; i < length; i++) {
        vm.profissionais.push(new Profissional(response.data[i]));
      }
    });

  // Form

  var loadForm = function(index) {
    if (index > -1) {
      vm.data = new Minicurso(vm.list[index]);
    } else {
      vm.data = {};
    }
  }

  vm.clean = function() {
    vm.error = {};
    vm.indexEdit = -1;
    loadForm(vm.indexEdit);
  }

  vm.create = function() {
    vm.openForm = true;
    vm.openList = false;
    vm.openFormTurma = false;
    vm.openListTurma = false;
    vm.indexEdit = -1;
    loadForm(vm.indexEdit);
  }

  vm.edit = function(instance) {
    vm.openForm = true;
    vm.openList = false;
    vm.openFormTurma = false;
    vm.openListTurma = false;
    vm.indexEdit = vm.list.indexOf(instance);
    loadForm(vm.indexEdit);
  }

  vm.showList = function() {
    vm.openForm = false;
    vm.openList = true;
    vm.openFormTurma = false;
    vm.openListTurma = false;
  }

  vm.submit = function() {
    var instance = new Minicurso(vm.data);
    if (vm.form.$valid) {
      if (instance.pk) {
        instance.put()
          .then(function(response) {
            vm.list[vm.indexEdit] = instance;
            vm.message.push({
              texto: 'Atualizado com sucesso!',
              tipo: 'success'
            });
            vm.showList();
            vm.clean();
          }, function(reject) {
            vm.error = reject.data;
          });
      } else {
        instance.edicao = $stateParams.edicao;
        instance.post()
          .then(function(response) {
            vm.list.push(new Minicurso(response.data));
            vm.message.push({
              texto: 'Cadastrado com sucesso!',
              tipo: 'success'
            });
            vm.showList();
            vm.clean();
          }, function(reject) {
            vm.error = reject.data;
          });
      }
    }
  }

  // List
  Minicurso.all($stateParams.edicao)
    .then(function(response) {
      var length = response.data.length;
      for (var i = 0; i < length; i++) {
        vm.list.push(new Minicurso(response.data[i]));
      }
      vm.loading = true;
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      });
    });

  vm.destroy = function(instance) {
    instance.destroy()
      .then(function() {
        var index = vm.list.indexOf(instance);
        vm.list.splice(index, 1);
        vm.message.push({
          texto: 'Deletado com sucesso!',
          tipo: 'success'
        });
      }, function(reject) {
        vm.message.push({
          texto: reject.data,
          tipo: 'danger'
        });
      });
  }

  // Turmas

  var builderTurma = function(data) {
    var profissional = Extra.getForPk(vm.profissionais, data.profissional);
    var grupo = Extra.getForPk(vm.grupos, data.grupo);
    
    if (profissional) {
      data.nome_profissional = profissional.nome;
    }
    if (grupo) {
      data.nome_grupo = grupo.nome;
    }
    return new TurmaMinicurso(data);
  }

  var loadFormTurma = function(index) {
    if (index > -1) {
      vm.data = builderTurma(vm.listTurma[index]);
    } else {
      vm.data = {};
    }
  }

  vm.showTurmas = function(pk) {
    vm.loadingTurma = false;
    if (!(minicurso == pk)) {
      vm.listTurma = []
      minicurso = pk;
      TurmaMinicurso.all(pk)
        .then(function(response) {
          var length = response.data.length;
          for (var i = 0; i < length; i++) {
            vm.listTurma.push(builderTurma(response.data[i]));
          }
          vm.loadingTurma = true;
        }, function(reject) {
          vm.message.push({
            texto: reject.data.detail,
            tipo: 'danger'
          });
        });
    } else {
      vm.loadingTurma = true;
    }
    vm.showListTurma();
  }

  vm.cleanTurma = function() {
    vm.error = {};
    vm.indexEditTurma = -1;
    loadFormTurma(vm.indexEditTurma);
  }

  vm.createTurma = function() {
    vm.openForm = false;
    vm.openList = false;
    vm.openFormTurma = true;
    vm.openListTurma = false;
    vm.indexEditTurma = -1;
    loadFormTurma(vm.indexEditTurma);
  }

  vm.editTurma = function(instance) {
    vm.openForm = false;
    vm.openList = false;
    vm.openFormTurma = true;
    vm.openListTurma = false;
    vm.indexEditTurma = vm.listTurma.indexOf(instance);
    loadFormTurma(vm.indexEditTurma);
  }

  vm.showListTurma = function() {
    vm.openForm = false;
    vm.openList = false;
    vm.openFormTurma = false;
    vm.openListTurma = true;
  }

  vm.submitTurma = function() {
    var instance = builderTurma(vm.data);
    if (vm.form.$valid) {
      if (instance.pk) {
        instance.put()
          .then(function(response) {
            vm.listTurma[vm.indexEditTurma] = instance;
            vm.message.push({
              texto: 'Atualizado com sucesso!',
              tipo: 'success'
            });
            vm.showListTurma();
            vm.cleanTurma();
          }, function(reject) {
            vm.error = reject.data;
          });
      } else {
        instance.minicurso = minicurso;
        instance.post()
          .then(function(response) {
            vm.listTurma.push(builderTurma(response.data));
            vm.message.push({
              texto: 'Cadastrado com sucesso!',
              tipo: 'success'
            });
            vm.showListTurma();
            vm.cleanTurma();
          }, function(reject) {
            vm.error = reject.data;
          });
      }
    }
  }
  vm.destroyTurma = function(instance) {
    instance.destroy()
      .then(function() {
        var index = vm.listTurma.indexOf(instance);
        vm.listTurma.splice(index, 1);
        vm.message.push({
          texto: 'Deletado com sucesso!',
          tipo: 'success'
        });
      }, function(reject) {
        vm.message.push({
          texto: reject.data,
          tipo: 'danger'
        });
      });
  }

  // Horário

  vm.loadingHorario = false;
  vm.messageHorario = [];
  vm.listHorario = [];
  vm.indexEditHorario = -1;

  var turma = 0;

  // Form

  vm.cleanMessageHorario = function() {
    vm.messageHorario = [];
  }

  vm.cleanHorario = function() {
    vm.data = {};
    vm.error = {};
    vm.indexEditHorario = -1;
  }

  vm.editHorario = function(instance) {
    vm.indexEditHorario = vm.listHorario.indexOf(instance);
    vm.data = new HorarioMinicurso(instance);
  }

  vm.submitHorario = function() {
    var instance = new HorarioMinicurso(vm.data);
    if (vm.form.$valid) {
      if (instance.pk) {
        instance.put()
          .then(function(response) {
            vm.listHorario[vm.indexEditHorario] = instance;
            vm.messageHorario.push({
              texto: 'Atualizado com sucesso!',
              tipo: 'success'
            });
            vm.cleanHorario();
          }, function(reject) {
            vm.error = reject.data;
          })
      } else {
        instance.turma = turma;
        instance.post()
          .then(function(response) {
            vm.listHorario.push(new HorarioMinicurso(response.data));
            vm.messageHorario.push({
              texto: 'Cadastrado com sucesso!',
              tipo: 'success'
            });
            vm.cleanHorario();
          }, function(reject) {
            vm.error = reject.data;
          });
      }
    }
  }

  // List

  vm.getHorario = function(instance) {
    turma = instance.pk;
    vm.listHorario = [];
    vm.cleanMessageHorario();
    vm.cleanHorario();
    HorarioMinicurso.all(instance.pk)
      .then(function(response) {
        var length = response.data.length;
        for (var i = 0; i < length; i++) {
          vm.listHorario.push(new HorarioMinicurso(response.data[i]));
        }
        vm.loadingHorario = true;
      }, function(reject) {
        vm.messageHorario.push({
          texto: reject.data.detail,
          tipo: 'danger'
        });
      });
  }

  vm.destroyHorario = function(instance) {
    instance.destroy()
      .then(function() {
        var index = vm.listHorario.indexOf(instance);
        vm.listHorario.splice(index, 1);
        vm.messageHorario.push({
          texto: 'Deletado com sucesso!',
          tipo: 'success'
        });
      }, function(reject) {
        vm.messageHorario.push({
          texto: reject.data,
          tipo: 'danger'
        });
      });
  }
}