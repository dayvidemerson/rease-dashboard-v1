'use strict';
angular.module('rease')
  .controller('PalestraController', PalestraController);

PalestraController.$inject = ['Palestra', 'Profissional', 'Extra', '$stateParams'];

function PalestraController(Palestra, Profissional, Extra, $stateParams) {
  var vm = this;
  vm.loading = false;
  vm.message = [];
  vm.profissionais = [];
  vm.data = {};
  vm.error = {};
  vm.list = [];
  vm.openForm = false;
  vm.openList = true;
  vm.indexEdit = -1;

  // Form

  Profissional.all()
    .then(function(response) {
      vm.profissionais = response.data;
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      });
    });

  var builder = function(data) {
    var profissional = Extra.getForPk(vm.profissionais, data.profissional);

    if (profissional) {
      data.nome_profissional = profissional.nome;
    }
    return new Palestra(data);
  }

  var loadForm = function(index) {
    if (index > -1) {
      vm.data = builder(vm.list[index]);
    } else {
      vm.data = {};
    }
  }

  vm.clean = function() {
    vm.error = {};
    vm.indexEdit = -1;
    loadForm(vm.indexEdit);
  }

  vm.create = function() {
    vm.openForm = true;
    vm.openList = false;
    vm.indexEdit = -1;
    loadForm(vm.indexEdit);
  }

  vm.edit = function(instance) {
    vm.openForm = true;
    vm.openList = false;
    vm.indexEdit = vm.list.indexOf(instance);
    loadForm(vm.indexEdit);
  }

  vm.showList = function() {
    vm.openForm = false;
    vm.openList = true;
  }

  vm.submit = function() {
    var instance = builder(vm.data);
    if (vm.form.$valid) {
      if (instance.pk) {
        instance.put()
          .then(function(response) {
            vm.list[vm.indexEdit] = builder(response.data);
            vm.message.push({
              texto: 'Atualizado com sucesso!',
              tipo: 'success'
            });
            vm.showList();
            vm.clean();
          }, function(reject) {
            vm.error = reject.data;
          });
      } else {
        instance.edicao = $stateParams.edicao;
        instance.post()
          .then(function(response) {
            vm.list.push(builder(response.data));
            vm.message.push({
              texto: 'Cadastrado com sucesso!',
              tipo: 'success'
            });
            vm.showList();
            vm.clean();
          }, function(reject) {
            vm.error = reject.data;
          });
      }
    }
  }

  // List

  Palestra.all($stateParams.edicao)
    .then(function(response) {
      var length = response.data.length
      for (var i = 0; i < length; i++) {
        vm.list.push(builder(response.data[i]))
      }
      vm.loading = true
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })

  vm.destroy = function(instance) {
    instance.destroy()
      .then(function() {
        var index = vm.list.indexOf(instance)
        vm.list.splice(index, 1)
        vm.message.push({
          texto: 'Deletado com sucesso!',
          tipo: 'success'
        })
      }, function(reject) {
        vm.message.push({
          texto: reject.data,
          tipo: 'danger'
        })
      })
  }
}