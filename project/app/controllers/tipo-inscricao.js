'use strict';
angular.module('rease')
  .controller('TipoInscricaoController', TipoInscricaoController);

TipoInscricaoController.$inject = ['TipoInscricao', 'TipoDocumento', '$stateParams'];

function TipoInscricaoController(TipoInscricao, TipoDocumento, $stateParams) {
  var vm = this;
  vm.loading = false;
  vm.message = [];
  vm.data = {};
  vm.error = {};
  vm.list = [];
  vm.openForm = false;
  vm.openList = true;
  vm.indexEdit = -1;

  // Form

  var loadForm = function(index) {
    if (index > -1) {
      vm.data = new TipoInscricao(vm.list[index]);
    } else {
      vm.data = {};
    }
  }

  vm.clean = function() {
    vm.error = {};
    vm.indexEdit = -1;
    loadForm(vm.indexEdit);
  }

  vm.create = function() {
    vm.openForm = true;
    vm.openList = false;
    vm.indexEdit = -1;
    loadForm(vm.indexEdit);
  }

  vm.edit = function(instance) {
    vm.openForm = true;
    vm.openList = false;
    vm.indexEdit = vm.list.indexOf(instance);
    loadForm(vm.indexEdit);
  }

  vm.showList = function() {
    vm.openForm = false;
    vm.openList = true;
  }

  vm.submit = function() {
    var instance = new TipoInscricao(vm.data)
    if (vm.form.$valid) {
      if (instance.pk) {
        instance.put()
          .then(function(response) {
            vm.list[vm.indexEdit] = instance
            vm.message.push({
              texto: 'Atualizado com sucesso!',
              tipo: 'success'
            })
            vm.showList();
            vm.clean()
          }, function(reject) {
            vm.error = reject.data
          })
      } else {
        instance.edicao = $stateParams.edicao
        instance.post()
          .then(function(response) {
            vm.list.push(new TipoInscricao(response.data))
            vm.message.push({
              texto: 'Cadastrado com sucesso!',
              tipo: 'success'
            })
            vm.showList();
            vm.clean()
          }, function(reject) {
            vm.error = reject.data
          })
      }
    }
  }

  // List

  TipoInscricao.all($stateParams.edicao)
    .then(function(response) {
      var length = response.data.length
      for (var i = 0; i < length; i++) {
        vm.list.push(new TipoInscricao(response.data[i]))
      }
      vm.loading = true
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })

  vm.destroy = function(instance) {
    instance.destroy()
      .then(function() {
        var index = vm.list.indexOf(instance)
        vm.list.splice(index, 1)
        vm.message.push({
          texto: 'Deletado com sucesso!',
          tipo: 'success'
        })
      }, function(reject) {
        vm.message.push({
          texto: reject.data,
          tipo: 'danger'
        })
      })
  }

  // Tipo de Documento

  vm.loadingTipoDocumento = false
  vm.messageTipoDocumento = []
  vm.dataTipoDocumento = {}
  vm.errorTipoDocumento = {}
  vm.listTipoDocumento = []
  vm.indexEditTipoDocumento = -1

  // Form

  vm.cleanMessageTipoDocumento = function() {
    vm.messageTipoDocumento = []
  }

  vm.cleanTipoDocumento = function() {
    vm.dataTipoDocumento = {}
    vm.errorTipoDocumento = {}
    vm.indexEditTipoDocumento = -1
  }

  vm.editTipoDocumento = function(instance) {
    vm.indexEditTipoDocumento = vm.listTipoDocumento.indexOf(instance)
    vm.dataTipoDocumento = new TipoDocumento(instance)
  }

  vm.submitTipoDocumento = function(object) {
    var instance = new TipoDocumento(vm.dataTipoDocumento)
    if (vm.formTipoDocumento.$valid) {
      if (instance.pk) {
        instance.put()
          .then(function(response) {
            vm.listTipoDocumento[vm.indexEditTipoDocumento] = instance
            vm.messageTipoDocumento.push({
              texto: 'Atualizado com sucesso!',
              tipo: 'success'
            })
            vm.cleanTipoDocumento()
          }, function(reject) {
            vm.errorTipoDocumento = reject.data
          })
      } else {
        instance.tipo_inscricao = object.pk
        instance.post()
          .then(function(response) {
            vm.listTipoDocumento.push(new TipoDocumento(response.data))
            vm.messageTipoDocumento.push({
              texto: 'Cadastrado com sucesso!',
              tipo: 'success'
            })
            vm.cleanTipoDocumento()
          }, function(reject) {
            vm.errorTipoDocumento = reject.data
          })
      }
    }
  }

  // List

  vm.getTipoDocumento = function(instance) {
    vm.listTipoDocumento = []
    vm.cleanMessageTipoDocumento()
    vm.cleanTipoDocumento()
    TipoDocumento.all(instance.pk)
      .then(function(response) {
        var length = response.data.length
        for (var i = 0; i < length; i++) {
          vm.listTipoDocumento.push(new TipoDocumento(response.data[i]))
        }
        vm.loadingTipoDocumento = true
      }, function(reject) {
        vm.messageTipoDocumento.push({
          texto: reject.data.detail,
          tipo: 'danger'
        })
      })
  }    

  vm.destroyTipoDocumento = function(instance) {
    instance.destroy()
      .then(function() {
        var index = vm.listTipoDocumento.indexOf(instance)
        vm.listTipoDocumento.splice(index, 1)
        vm.messageTipoDocumento.push({
          texto: 'Deletado com sucesso!',
          tipo: 'success'
        })
      }, function(reject) {
        vm.messageTipoDocumento.push({
          texto: reject.data,
          tipo: 'danger'
        })
      })
  }
}