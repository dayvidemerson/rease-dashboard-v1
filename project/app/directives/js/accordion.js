'use strict'
angular.module('rease')
  .directive('accordion', accordion)

  function accordion() {
    var ddo = {}
    ddo.restric = 'E'
    ddo.scope = {
      name: '@',
      tipo: '@',
      titulo: '@'
    }
    ddo.transclude = true
    ddo.templateUrl = '/app/directives/html/accordion.html'
    return ddo
  }