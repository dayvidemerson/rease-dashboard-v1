'use strict'
angular.module('rease')
  .directive('confirmClick', confirmClick)
  
  function confirmClick(){
    return {
      link: function (scope, element, attr) {
        var msg = attr.confirmClick || 'Você tem certeza?'
        var clickAction = attr.confirmedClick
        element.bind('click',function (event) {
          if (window.confirm(msg)) {
            scope.$eval(clickAction)
          }
        })
      }
    }
  }