'use strict'
angular.module('rease')
  .directive('fileReader', fileReader)

fileReader.$inject = ['$q']

function fileReader($q) {
  var slice = Array.prototype.slice

  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$render = function() {}

      element.bind('change', function(e) {
        var element = e.target

        $q.all(slice.call(element.files, 0).map(readFile))
          .then(function(values) {
            if (element.files[0].size <= 1048576) {
              ngModel.$setViewValue(values.length ? values[0] : null)
              ngModel.$setValidity('size', true)
            } else {
              ngModel.$setValidity('size', false)
            }
          })

        function readFile(file) {
          var deferred = $q.defer()

          var reader = new FileReader()
          reader.onload = function(e) {
            // img = new Image()
            // img.onload = function() {
            //   if (img.width <= 300) ngModel.$setValidity('width', false)
            //   else ngModel.$setValidity('width', true)

            //   if (img.height <= 300) ngModel.$setValidity('height', false)
            //   else ngModel.$setValidity('height', true)
            // }
            // img.src = reader.result
            deferred.resolve(e.target.result)
          }
          reader.onerror = function(e) {
            deferred.reject(e)
          }
          reader.readAsDataURL(file)

          return deferred.promise
        }

      })

    }
  }
}