'use strict'
angular.module('rease')
  .directive('modalButtonOpen', modalButtonOpen)
  .directive('modalButtonClose', modalButtonClose)
  .directive('modal', modal)
  .directive('modalBody', modalBody)
  .directive('modalFooter', modalFooter)

  function modalButtonOpen() {
    var ddo = {};
    ddo.restric = 'E';
    ddo.scope = {
      tipo: '@',
      target: '@'
    }
    ddo.transclude = true;
    ddo.templateUrl = 'app/directives/html/modal-button.html';
    return ddo;
  }

  function modalButtonClose() {
    var ddo = {};
    ddo.restric = 'E';
    ddo.transclude = true;
    ddo.template = '<button type=\'button\' class=\'btn btn-default\' data-dismiss=\'modal\' ng-transclude></button>';
    return ddo;
  }

  function modal() {
    var ddo = {};
    ddo.restric = 'E';
    ddo.scope = {
      titulo: '@',
      target: '@'
    }
    ddo.transclude = true;
    ddo.templateUrl = 'app/directives/html/modal.html';
    return ddo;
  }

  function modalBody() {
    var ddo = {};
    ddo.restric = 'E';
    ddo.transclude = true;
    ddo.template = '<div class=\'modal-body\' ng-transclude></div>';
    return ddo;
  }

  function modalFooter() {
    var ddo = {};
    ddo.restric = 'E';
    ddo.transclude = true;
    ddo.template = '<div class=\'modal-footer\' ng-transclude></div>';
    return ddo;
  }