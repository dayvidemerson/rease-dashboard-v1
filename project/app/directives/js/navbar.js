'use strict'
angular.module('rease')
  .directive('navbar', navbar)

  function navbar() {
    var ddo = {};
    ddo.restric = 'E';
    ddo.scope = {
      tipo: '@'
    }
    ddo.transclude = true
    ddo.templateUrl = 'app/directives/html/navbar.html'
    return ddo;
  }