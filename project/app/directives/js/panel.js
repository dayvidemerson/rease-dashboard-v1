'use strict'
angular.module('rease')
  .directive('panel', panel)
  .directive('panelBody', panelBody)
  .directive('panelFooter', panelFooter)

  function panel() {
    var ddo = {};
    ddo.restric = 'E';
    ddo.scope = {
      titulo: '@',
      tipo: '@'
    }
    ddo.transclude = true;
    ddo.templateUrl = 'app/directives/html/panel.html';
    return ddo;
  }

  function panelBody() {
    var ddo = {};
    ddo.restric = 'E';
    ddo.transclude = true;
    ddo.template = '<div class=\'panel-body\' ng-transclude></div>';
    return ddo;
  }

  function panelFooter() {
    var ddo = {};
    ddo.restric = 'E';
    ddo.transclude = true;
    ddo.template = '<div class=\'panel-footer\' ng-transclude></div>';
    return ddo;
  }