'use strict'
angular.module('rease')
  .factory('Area', Area)
 
  Area.$inject = ['api', '$http']

  function Area(api, $http) {
    var url = api+'/administrador/areas/'

    var Area = function(data) {
      this.pk = data.pk
      this.nome = data.nome
    }

    Area.prototype = {
      post: function() {
        return $http.post(url, this.json())
      },
      put: function() {
        return $http.put(url+this.pk+'/', this.json())
      },
      destroy: function() {
        return $http.delete(url+this.pk+'/')
      },
      json: function() {
        return JSON.stringify(this)
      }
    }

    Area.all = function() {
      return $http.get(url)
    }

    Area.get = function(pk) {
      return $http.get(url+pk+'/')
    }

    return Area
  }