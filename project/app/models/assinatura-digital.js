'use strict'
angular.module('rease')
  .factory('AssinaturaDigital', AssinaturaDigital)
 
  AssinaturaDigital.$inject = ['api', '$http']

  function AssinaturaDigital(api, $http) {
    var url = api+'/administrador/assinaturas/'

    var AssinaturaDigital = function(data) {
      this.pk = data.pk
      this.nome = data.nome
      this.cargo = data.cargo
      this.imagem = data.imagem
    }

    AssinaturaDigital.prototype = {
      post: function() {
        return $http.post(url, this.json())
      },
      put: function() {
        return $http.put(url+this.pk+'/', this.json())
      },
      destroy: function() {
        return $http.delete(url+this.pk+'/')
      },
      json: function() {
        return JSON.stringify(this)
      }
    }

    AssinaturaDigital.all = function() {
      return $http.get(url)
    }

    AssinaturaDigital.get = function(pk) {
      return $http.get(url+pk+'/')
    }

    return AssinaturaDigital
  }