'use strict';
angular.module('rease')
  .factory('AtividadeGeral', AtividadeGeral);
 
AtividadeGeral.$inject = ['api', '$http', '$filter', 'Time'];

function AtividadeGeral(api, $http, $filter, Time) {
  var url = api+'/administrador/atividades/geral/';

  var AtividadeGeral = function(data) {
    this.pk = data.pk;
    this.descricao = data.descricao;
    this.carga_horaria = data.carga_horaria;
    this.data = Time.date(data.data);
    this.inicio = Time.time(data.inicio);
    this.termino = Time.time(data.termino);
    this.edicao = data.edicao;
  }

  AtividadeGeral.prototype = {
    post: function() {
      return $http.post(url, this.json());
    },
    put: function() {
      return $http.put(url+this.pk+'/', this.json());
    },
    destroy: function() {
      return $http.delete(url+this.pk+'/');
    },
    json: function() {
      var json = this;
      json.data = $filter('date')(json.data, 'yyyy-MM-dd');
      json.inicio = $filter('date')(json.inicio, 'HH:mm');
      json.termino = $filter('date')(json.termino, 'HH:mm');
      return JSON.stringify(json);
    }
  }

  AtividadeGeral.all = function(pk) {
    return $http.get(url, {
      params: {
        'edicao': pk
      }
    });
  }

  AtividadeGeral.get = function(pk) {
    return $http.get(url+pk+'/');
  }

  AtividadeGeral.relatorio = function(pk) {
    return $http.get(url+'relatorio/'+pk+'/');
  }

  return AtividadeGeral;
}