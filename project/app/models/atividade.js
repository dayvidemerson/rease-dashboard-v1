'use strict';
angular.module('rease')
  .factory('Atividade', Atividade);
 
Atividade.$inject = ['api', '$http'];

function Atividade(api, $http) {
  var url = api+'/administrador/atividades/';

  var Atividade = function(data) {
    this.pk = data.pk;
    this.nome = data.nome;
    this.descricao = data.descricao;
    this.carga_horaria = data.carga_horaria;
    this.edicao = data.edicao;
  }

  Atividade.prototype = {
    post: function() {
      return $http.post(url, this.json());
    },
    put: function() {
      return $http.put(url+this.pk+'/', this.json());
    },
    destroy: function() {
      return $http.delete(url+this.pk+'/');
    },
    json: function() {
      return JSON.stringify(this);
    }
  }

  Atividade.all = function(pk) {
    return $http.get(url, {
      params: {
        'edicao': pk
      }
    });
  }

  Atividade.get = function(pk) {
    return $http.get(url+pk+'/');
  }

  Atividade.relatorio = function(pk) {
    return $http.get(url+'relatorio/'+pk+'/');
  }

  return Atividade;
}