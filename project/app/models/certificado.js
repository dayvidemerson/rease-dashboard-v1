'use strict'
angular.module('rease')
  .factory('Certificado', Certificado)
 
  Certificado.$inject = ['api', '$http']

  function Certificado(api, $http) {
    var url = api+'/administrador/certificados/'

    var Certificado = function(data) {
      this.pk = data.pk
      this.disponivel = data.disponivel
    }

    Certificado.prototype = {
      put: function() {
        return $http.put(url+this.pk+'/', this.json())
      },
      json: function() {
        return JSON.stringify(this)
      }
    }

    return Certificado
  }