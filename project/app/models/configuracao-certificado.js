'use strict'
angular.module('rease')
  .factory('ConfiguracaoCertificado', ConfiguracaoCertificado)
 
  ConfiguracaoCertificado.$inject = ['api', '$http']

  function ConfiguracaoCertificado(api, $http) {
    var url = api+'/administrador/configuracoes/certificado/'

    var ConfiguracaoCertificado = function(data) {
      this.pk = data.pk
      this.livro_certificado = data.livro_certificado
      this.quantidade_linha_pagina = data.quantidade_linha_pagina
      this.numero_inicio_certificado = data.numero_inicio_certificado
      this.pagina_inicio_certificado = data.pagina_inicio_certificado
      this.assinatura_carimbo = data.assinatura_carimbo
      this.edicao = data.edicao
    }

    ConfiguracaoCertificado.prototype = {
      post: function() {
        return $http.post(url, this.json())
      },
      put: function() {
        return $http.put(url+this.pk+'/', this.json())
      },
      destroy: function() {
        return $http.delete(url+this.pk+'/')
      },
      json: function() {
        return JSON.stringify(this)
      }
    }

    ConfiguracaoCertificado.all = function(pk) {
      return $http.get(url, {
        params: {
          'edicao': pk
        }
      })
    }

    ConfiguracaoCertificado.get = function(pk) {
      return $http.get(url+pk+'/')
    }

    return ConfiguracaoCertificado
  }