'use strict'
angular.module('rease')
  .factory('Documento', Documento)
 
  Documento.$inject = ['api', '$http']

  function Documento(api, $http) {
    var url = api+'/administrador/documentos/'

    var Documento = function(data) {
      this.pk = data.pk
      this.tipo = data.tipo
      this.documento = data.documento
      this.valido = data.valido
    }

    Documento.prototype = {
      validate: function(validate) {
        return $http.put(url+this.pk+'/', {validate: validate})
      }
    }
    return Documento
  }