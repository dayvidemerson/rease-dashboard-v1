'use strict';
angular.module('rease')
.factory('Edicao', Edicao);

Edicao.$inject = ['api', 'Time', 'Extra', '$http', '$filter'];

function Edicao(api, Time, Extra, $http, $filter) {
    var url = api + '/administrador/edicoes/';

    var Edicao = function(data) {
        this.pk = data.pk;
        this.nome = data.nome;
        this.edicao = data.edicao;
        this.cep = data.cep;
        this.logradouro = data.logradouro;
        this.numero = data.numero;
        this.bairro = data.bairro;
        this.cidade = data.cidade;
        this.estado = data.estado;
        this.complemento = data.complemento;
        this.evento = data.evento;
        this.status = data.status;
        this.certificado = data.certificado;
        this.inicio = Time.date(data.inicio);
        this.termino = Time.date(data.termino);
        this.inicio_inscricao = Time.date(data.inicio_inscricao);
        this.termino_inscricao = Time.date(data.termino_inscricao);
        this.areas = data.areas;
    };

    Edicao.prototype = {
        post: function() {
            return $http.post(url, this.json());
        },
        put: function() {
            return $http.put(url + this.pk + '/', this.json());
        },
        destroy: function() {
            return $http.delete(url + this.pk + '/');
        },
        dict: function() {
            var dict = this;
            dict.inicio = $filter('date')(dict.inicio, 'yyyy-MM-dd');
            dict.termino = $filter('date')(dict.termino, 'yyyy-MM-dd');
            dict.inicio_inscricao = $filter('date')(dict.inicio_inscricao, 'yyyy-MM-dd');
            dict.termino_inscricao = $filter('date')(dict.termino_inscricao, 'yyyy-MM-dd');
            return dict;
        },
        json: function() {
            return JSON.stringify(this.dict());
        }
    };

    Edicao.all = function(pk) {
        return $http.get(url, {
            params: {
                'evento': pk
            }
        });
    };

    Edicao.get = function(pk) {
        return $http.get(url + pk + '/');
    };

    Edicao.relatorio = function(pk) {
        return $http.get(url + 'relatorio/' + pk + '/');
    };

    return Edicao;
}
