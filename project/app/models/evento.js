'use strict'
angular.module('rease')
  .factory('Evento', Evento)
 
  Evento.$inject = ['api', '$http']

  function Evento(api, $http) {
    var url = api+'/administrador/eventos/'

    var Evento = function(data) {
      this.pk = data.pk
      this.nome = data.nome
      this.sigla = data.sigla
    }

    Evento.prototype = {
      post: function() {
        return $http.post(url, this.json())
      },
      put: function() {
        return $http.put(url+this.pk+'/', this.json())
      },
      destroy: function() {
        return $http.delete(url+this.pk+'/')
      },
      json: function() {
        return JSON.stringify(this)
      }
    }

    Evento.all = function() {
      return $http.get(url)
    }

    Evento.get = function(pk) {
      return $http.get(url+pk+'/')
    }

    return Evento
  }