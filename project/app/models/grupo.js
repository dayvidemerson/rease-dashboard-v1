'use strict'
angular.module('rease')
  .factory('Grupo', Grupo)
 
  Grupo.$inject = ['api', '$http']

  function Grupo(api, $http) {
    var url = api+'/administrador/grupos/'

    var Grupo = function(data) {
      this.pk = data.pk
      this.nome = data.nome
      this.edicao = data.edicao
    }

    Grupo.prototype = {
      post: function() {
        return $http.post(url, this.json())
      },
      put: function() {
        return $http.put(url+this.pk+'/', this.json())
      },
      destroy: function() {
        return $http.delete(url+this.pk+'/')
      },
      json: function() {
        return JSON.stringify(this)
      }
    }

    Grupo.all = function(pk) {
      return $http.get(url, {
        params: {
          'edicao': pk
        }
      })
    }

    Grupo.get = function(pk) {
      return $http.get(url+pk+'/')
    }

    return Grupo
  }