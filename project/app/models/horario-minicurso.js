'use strict'
angular.module('rease')
  .factory('HorarioMinicurso', HorarioMinicurso)
 
  HorarioMinicurso.$inject = ['api', 'Time', '$http', '$filter']

  function HorarioMinicurso(api, Time, $http, $filter) {
    var url = api+'/administrador/horarios/minicurso/'

    var HorarioMinicurso = function(data) {
      this.pk = data.pk
      this.turma = data.turma
      this.data = Time.date(data.data)
      this.inicio = Time.time(data.inicio)
      this.termino = Time.time(data.termino)
    }

    HorarioMinicurso.prototype = {
      post: function() {
        return $http.post(url, this.json())
      },
      put: function() {
        return $http.put(url+this.pk+'/', this.json())
      },
      destroy: function() {
        return $http.delete(url+this.pk+'/')
      },
      json: function() {
        var json = this
        json.data = $filter('date')(json.data, 'yyyy-MM-dd')
        json.inicio = $filter('date')(json.inicio, 'HH:mm')
        json.termino = $filter('date')(json.termino, 'HH:mm')
        return JSON.stringify(json)
      }
    }

    HorarioMinicurso.all = function(pk) {
      return $http.get(url, {
        params: {
          'turma': pk
        }
      })
    }

    HorarioMinicurso.get = function(pk) {
      return $http.get(url+pk+'/')
    }

    return HorarioMinicurso
  }