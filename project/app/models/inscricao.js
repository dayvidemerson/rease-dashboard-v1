'use strict'
angular.module('rease')
  .factory('Inscricao', Inscricao)
 
  Inscricao.$inject = ['api', '$http', 'Documento']

  function Inscricao(api, $http, Documento) {
    var url = api+'/administrador/inscricoes/'

    var Inscricao = function(data) {
      this.pk = data.pk
      this.participante = data.participante
      this.email = data.email
      this.tipo = data.tipo
      this.preco = data.preco
      this.status = data.status
      this.documentos = []
      if (data.documentos) {
        var length = data.documentos.length
        for (var i = 0; i < length; i++) {
          this.documentos.push(new Documento(data.documentos[i]))
        }
      }
      this.certificado_evento = data.certificado_evento
      this.certificado_disponivel = data.certificado_disponivel
      this.minicursos = data.minicursos
      this.palestras = data.palestras
      this.atividades = data.atividades
    }

    Inscricao.prototype = {
      active: function(active) {
        return $http.put(url+this.pk+'/', {active: active})
      }
    }

    Inscricao.all = function(pk) {
      return $http.get(url, {
        params: {
          'tipo__edicao': pk
        }
      })
    }

    Inscricao.getIndexDocumento = function(lista, pk) {
      var inscricoes_length = lista.length
      for (var i = 0; i < inscricoes_length; i++) {
        var documentos_length = lista[i].documentos.length
        for (var y = 0; y < documentos_length; y++) {
          if (lista[i].documentos[y].pk == pk) {
            return [i, y]
          }
        }
      }
      return []
    }

    return Inscricao
  }