'use strict';
angular.module('rease')
  .factory('Minicurso', Minicurso);
 
Minicurso.$inject = ['api', '$http'];

function Minicurso(api, $http) {
  var url = api+'/administrador/minicursos/';

  var Minicurso = function(data) {
    this.pk = data.pk;
    this.nome = data.nome;
    this.carga_horaria = data.carga_horaria;
    this.edicao = data.edicao;
  }

  Minicurso.prototype = {
    post: function() {
      return $http.post(url, this.json());
    },
    put: function() {
      return $http.put(url+this.pk+'/', this.json());
    },
    destroy: function() {
      return $http.delete(url+this.pk+'/');
    },
    dict: function() {
      return this;
    },
    json: function() {
      return JSON.stringify(this.dict());
    }
  }

  Minicurso.all = function(pk) {
    return $http.get(url, {
      params: {
        'edicao': pk
      }
    });
  }

  Minicurso.get = function(pk) {
    return $http.get(url+pk+'/');
  }

  Minicurso.relatorio = function(pk) {
    return $http.get(url+'relatorio/'+pk+'/');
  }

  return Minicurso;
}