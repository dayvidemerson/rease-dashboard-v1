'use strict'
angular.module('rease')
  .factory('ModeloCertificadoPalestra', ModeloCertificadoPalestra)
 
  ModeloCertificadoPalestra.$inject = ['api', '$http']

  function ModeloCertificadoPalestra(api, $http) {
    var url = api+'/administrador/modelos/palestra/'

    var ModeloCertificadoPalestra = function(data) {
      this.pk = data.pk
      this.background_frente = data.background_frente
      this.background_costa = data.background_costa
      this.texto = data.texto
      this.assinatura_diretor = data.assinatura_diretor
      this.assinatura_coordenador = data.assinatura_coordenador
      this.edicao = data.edicao
    }

    ModeloCertificadoPalestra.prototype = {
      post: function() {
        return $http.post(url, this.json())
      },
      put: function() {
        return $http.put(url+this.pk+'/', this.json())
      },
      destroy: function() {
        return $http.delete(url+this.pk+'/')
      },
      json: function() {
        return JSON.stringify(this)
      }
    }

    ModeloCertificadoPalestra.all = function(pk) {
      return $http.get(url, {
        params: {
          'edicao': pk
        }
      })
    }

    ModeloCertificadoPalestra.get = function(pk) {
      return $http.get(url+pk+'/')
    }

    return ModeloCertificadoPalestra
  }