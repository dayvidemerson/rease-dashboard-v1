'use strict';
angular.module('rease')
  .factory('Palestra', Palestra);
 
Palestra.$inject = ['api', 'Time', '$http', '$filter'];

function Palestra(api, Time, $http, $filter) {
  var url = api+'/administrador/palestras/';

  var Palestra = function(data) {
    this.pk = data.pk;
    this.nome = data.nome;
    this.hora = Time.time(data.hora);
    this.data = Time.date(data.data);
    this.carga_horaria = data.carga_horaria;
    this.profissional = data.profissional;
    this.nome_profissional = data.nome_profissional;
    this.edicao = data.edicao;
  }

  Palestra.prototype = {
    post: function() {
      return $http.post(url, this.json());
    },
    put: function() {
      return $http.put(url+this.pk+'/', this.json());
    },
    destroy: function() {
      return $http.delete(url+this.pk+'/');
    },
    json: function() {
      var json = this
      json.hora = $filter('date')(json.hora, 'HH:mm');
      json.data = $filter('date')(json.data, 'yyyy-MM-dd');
      return JSON.stringify(json);
    }
  }

  Palestra.all = function(pk) {
    return $http.get(url, {
      params: {
        'edicao': pk
      }
    });
  }

  Palestra.get = function(pk) {
    return $http.get(url+pk+'/');
  }

  Palestra.relatorio = function(pk) {
    return $http.get(url+'relatorio/'+pk+'/');
  }

  return Palestra;
}