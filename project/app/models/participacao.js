'use strict'
angular.module('rease')
  .factory('Participacao', Participacao)
 
  Participacao.$inject = ['api', '$http']

  function Participacao(api, $http) {
    var url = api+'/administrador/participacao/'

    var Participacao = function(data) {
      this.pk = data.pk
      this.inscricao = data.inscricao
      this.atividade_geral = data.atividade_geral
    }

    Participacao.prototype = {
      post: function() {
        return $http.post(url, this.json())
      },
      put: function() {
        return $http.put(url+this.pk+'/', this.json())
      },
      destroy: function() {
        return $http.delete(url+this.pk+'/')
      },
      json: function() {
        return JSON.stringify(this)
      }
    }

    Participacao.all = function() {
      return $http.get(url)
    }

    Participacao.get = function(pk) {
      return $http.get(url+pk+'/')
    }

    return Participacao
  }