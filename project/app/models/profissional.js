'use strict'
angular.module('rease')
  .factory('Profissional', Profissional)
 
  Profissional.$inject = ['api', '$http']

  function Profissional(api, $http) {
    var url = api+'/administrador/profissionais/'

    var Profissional = function(data) {
      this.pk = data.pk
      this.nome = data.nome
    }

    Profissional.prototype = {
      post: function() {
        return $http.post(url, this.json())
      },
      put: function() {
        return $http.put(url+this.pk+'/', this.json())
      },
      destroy: function() {
        return $http.delete(url+this.pk+'/')
      },
      json: function() {
        return JSON.stringify(this)
      }
    }

    Profissional.all = function() {
      return $http.get(url)
    }

    Profissional.get = function(pk) {
      return $http.get(url+pk+'/')
    }

    return Profissional
  }