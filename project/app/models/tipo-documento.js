'use strict'
angular.module('rease')
  .factory('TipoDocumento', TipoDocumento)
 
  TipoDocumento.$inject = ['api', '$http']

  function TipoDocumento(api, $http) {
    var url = api+'/administrador/tipos/documento/'

    var TipoDocumento = function(data) {
      this.pk = data.pk
      this.nome = data.nome
      this.tipo_inscricao = data.tipo_inscricao
    }

    TipoDocumento.prototype = {
      post: function() {
        return $http.post(url, this.json())
      },
      put: function() {
        return $http.put(url+this.pk+'/', this.json())
      },
      destroy: function() {
        return $http.delete(url+this.pk+'/')
      },
      json: function() {
        return JSON.stringify(this)
      }
    }

    TipoDocumento.all = function(pk) {
      return $http.get(url, {
        params: {
          'tipo_inscricao': pk,
        }
      })
    }

    TipoDocumento.get = function(pk) {
      return $http.get(url+pk+'/')
    }

    return TipoDocumento
  }