'use strict'
angular.module('rease')
  .factory('TipoInscricao', TipoInscricao)
 
  TipoInscricao.$inject = ['api', 'Time', '$http', '$filter']

  function TipoInscricao(api, Time, $http, $filter) {
    var url = api+'/administrador/tipos/inscricao/'

    var TipoInscricao = function(data) {
      this.pk = data.pk
      this.nome = data.nome
      this.preco_minicurso = data.preco_minicurso
      this.preco_palestra = data.preco_palestra
      this.preco_atividade = data.preco_atividade
      this.preco_minicurso_aumento = data.preco_minicurso_aumento
      this.preco_palestra_aumento = data.preco_palestra_aumento
      this.preco_atividade_aumento = data.preco_atividade_aumento
      this.edicao = data.edicao
      this.data_aumento = Time.date(data.data_aumento)
    }

    TipoInscricao.prototype = {
      post: function() {
        return $http.post(url, this.json())
      },
      put: function() {
        return $http.put(url+this.pk+'/', this.json())
      },
      destroy: function() {
        return $http.delete(url+this.pk+'/')
      },
      json: function() {
        var json = this
        json.data_aumento = $filter('date')(json.data_aumento, 'yyyy-MM-dd')
        return JSON.stringify(json)
      }
    }

    TipoInscricao.all = function(pk) {
      return $http.get(url, {
        params: {
          'edicao': pk
        }
      })
    }

    TipoInscricao.get = function(pk) {
      return $http.get(url+pk+'/')
    }

    return TipoInscricao
  }