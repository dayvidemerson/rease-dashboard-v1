'use strict';
angular.module('rease')
  .factory('TurmaAtividade', TurmaAtividade);
 
TurmaAtividade.$inject = ['api', '$http'];

function TurmaAtividade(api, $http) {
  var url = api+'/administrador/turmas/atividade/';

  var TurmaAtividade = function(data) {
    this.pk = data.pk;
    this.vagas = data.vagas;
    this.grupo = data.grupo;
    this.nome_grupo = data.nome_grupo;
    this.atividade = data.atividade;
  }

  TurmaAtividade.prototype = {
    post: function() {
      return $http.post(url, this.json());
    },
    put: function() {
      return $http.put(url+this.pk+'/', this.json());
    },
    destroy: function() {
      return $http.delete(url+this.pk+'/');
    },
    json: function() {
      return JSON.stringify(this);
    }
  }

  TurmaAtividade.all = function(pk) {
    return $http.get(url, {
      params: {
        'atividade': pk,
      }
    });
  }

  TurmaAtividade.get = function(pk) {
    return $http.get(url+pk+'/');
  }

  return TurmaAtividade;
}