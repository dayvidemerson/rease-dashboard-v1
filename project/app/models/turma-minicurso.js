'use strict';
angular.module('rease')
  .factory('TurmaMinicurso', TurmaMinicurso);
 
TurmaMinicurso.$inject = ['api', '$http'];

function TurmaMinicurso(api, $http) {
  var url = api+'/administrador/turmas/minicurso/';

  var TurmaMinicurso = function(data) {
    this.pk = data.pk;
    this.vagas = data.vagas;
    this.profissional = data.profissional;
    this.nome_profissional = data.nome_profissional;
    this.grupo = data.grupo;
    this.nome_grupo = data.nome_grupo;
    this.minicurso = data.minicurso;
  }

  TurmaMinicurso.prototype = {
    post: function() {
      return $http.post(url, this.json());
    },
    put: function() {
      return $http.put(url+this.pk+'/', this.json());
    },
    destroy: function() {
      return $http.delete(url+this.pk+'/');
    },
    json: function() {
      return JSON.stringify(this);
    }
  }

  TurmaMinicurso.all = function(pk) {
    return $http.get(url, {
      params: {
        'minicurso': pk,
      }
    });
  }

  TurmaMinicurso.get = function(pk) {
    return $http.get(url+pk+'/');
  }

  return TurmaMinicurso;
}