'use strict'
angular.module('ngAuthentication', [])
  .factory('AuthenticationService', AuthenticationService)
 
  AuthenticationService.$inject = ['api', '$http', '$cookies', '$rootScope']

  function AuthenticationService(api, $http, $cookies, $rootScope) {
    var service = {}
 
    service.get = get
    service.set = set
    service.destroy = destroy
    service.isLogged = isLogged
 
    return service
 
    function get(data) {
      return $http.post(api+'/api-token-auth/', data)
    }
 
    function set(data) {
      $rootScope.token = data.token
      $http.defaults.headers.common.Authorization = 'Token ' + data.token
      $cookies.put('token', data.token)
    }
 
    function destroy() {
      delete $rootScope.token
      $cookies.remove('token')
      $http.defaults.headers.common.Authorization = ''
    }

    function isLogged() {
      if ('token' in $rootScope && $http.defaults.headers.common.Authorization && $cookies.get('token')) {
        return true
      }
      return false
    }
  }