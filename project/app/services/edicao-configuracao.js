'use strict'
angular.module('rease')
  .factory('EdicaoConfiguracaoService', EdicaoConfiguracaoService)

  EdicaoConfiguracaoService.$inject = ['Edicao']

  function EdicaoConfiguracaoService(Edicao) {
    var service = {}
    var edicao = null
 
    service.setEdicao = setEdicao
    service.getEdicao = getEdicao

    return service
 
    function setEdicao(pk) {
      Edicao.get(pk)
        .then(function(response) {
          edicao = new Edicao(response.data)
        })
    }

    function getEdicao() {
      return edicao
    }

  }