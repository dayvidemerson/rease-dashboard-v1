'use strict'
angular.module('rease')
  .factory('Endereco', Endereco)

  Endereco.$inject = ['api', '$http']

  function Endereco(api, $http) {
    var service = {}
 
    service.getEstados = getEstados
    service.getLocalidade = getLocalidade

    return service
 
    function getEstados() {
      return $http.get(api+'/core/estados/')
    }

    function getLocalidade(cep) {
      return $http.jsonp('https://viacep.com.br/ws/'+cep+'/json/?callback=JSON_CALLBACK')
    }
  }