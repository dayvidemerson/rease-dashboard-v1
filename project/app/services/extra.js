'use strict'
angular.module('rease')
  .factory('Extra', Extra);
 
Extra.$inject = [];

function Extra() {
  var service = {};

  service.pks = pks;
  service.getForPk = getForPk;

  return service;

  function pks(list) {
    var length = list.length;
    var response = [];
    for (var i = 0; i < length; i++) {
      if (list[i].pk) {
        response.push(list[i].pk);
      } else {
        response.push(list[i]);
      }
    }
    return response;
  }

  function getForPk(list, pk) {
    var length = list.length;
    for (var i = 0; i < length; i++) {
      if (list[i].pk == pk) {
        return list[i];
      }
    }
    return null;
  }

  return Extra;
}