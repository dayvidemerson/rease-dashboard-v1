'use strict'
angular.module('rease')
  .factory('ListaPresencaService', ListaPresencaService)
 
ListaPresencaService.$inject = ['api', '$http']

function ListaPresencaService(api, $http) {
  var url = api+'/administrador/participantes/'
  var service = {}

  service.getMinicurso = getMinicurso
  service.getAtividade = getAtividade
  service.getPalestra = getPalestra

  return service

  function getMinicurso(pk) {
    return $http.get(url+'minicurso/'+pk+'/pdf/')
  }

  function getAtividade(pk) {
    return $http.get(url+'atividade/'+pk+'/pdf/')
  }

  function getPalestra(pk) {
    return $http.get(url+'palestra/'+pk+'/pdf/')
  }
}