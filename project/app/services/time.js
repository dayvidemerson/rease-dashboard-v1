'use strict'
angular.module('rease')
  .factory('Time', Time)
 
  Time.$inject = []
  function Time() {
    var service = {}

    service.date = date
    service.time = time

    return service

    function date(date) {
      if (date && !(date instanceof Date)) {
        var ano = date.slice(0, 4)
        var mes = date.slice(5, 7)
        var dia = date.slice(8, 10)
        return new Date(parseInt(ano), parseInt(mes)-1, parseInt(dia))
      }
      return date
    }

    function time(time) {
      if (time && !(time instanceof Date)) {
        var inicio = time.slice(0, 2)
        var minuto = time.slice(3, 5)
        return new Date(1970, 0, 1, inicio, minuto, 0)
      }
      return time
    }

    return Time
  }